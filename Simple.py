import subprocess
import sys

def run_bandit_scan(target_path):
    """Run the Bandit scan on a specified file or directory."""
    try:
        # Running Bandit as a subprocess
        result = subprocess.run(['bandit', '-r', target_path, '-f', 'txt'], text=True, capture_output=True)
        return result.stdout
    except Exception as e:
        return f"Failed to run Bandit: {str(e)}"

def main():
    if len(sys.argv)!=2:
     print("skahbdhjsadjas")
     sys.exit(1)

    

    target_path = sys.argv[1]
    print("Running security analysis on:", target_path)
    report = run_bandit_scan(target_path)
    print("Analysis Report:")
    print(report)

if __name__ == "__main__":
    main()
